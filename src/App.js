import React from 'react';
import { ThemeProvider } from 'styled-components';

import Stations from './containers/Stations';

import theme, { GlobalStyle } from './styles/theme';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <div className="App">
        <Stations />
      </div>
    </ThemeProvider>
  );
}

export default App;
