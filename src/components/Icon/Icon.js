import React from 'react';
import styled from 'styled-components';

import * as paths from './paths';

const SVG = styled.svg``;


const Icon = ({ className, icon }) => (
  <SVG
    className={className}
    focusable={false}
    viewBox="0 0 24 24" aria
    hidden={true}
    width="16"
  >
    <path d={paths[icon] || icon} />
  </SVG>
);

Icon.icons = paths;

export default Icon;
