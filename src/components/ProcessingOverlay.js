import styled from 'styled-components';

const ProcessingOverlay = styled.div`
  align-items: center;
  background-color: white;
  display: flex;
  height: 100vh;
  justify-content: center;
  left: 0;
  opacity: 0.8;
  position: absolute;
  top: 0;
  width: 100vw;
  ${({ theme }) => theme.fontSizes.l};
`;

export default ProcessingOverlay;
