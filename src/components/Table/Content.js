import styled from 'styled-components';

import Row from './Row';

export const ContentLayout = styled.div`
  display: grid;
  row-gap: ${({ theme }) => theme.spacing.s};
`;

const Content = ({ data, onClick }) => (
  <ContentLayout>
    {data.map(station => (
      <Row {...station} onClick={() => onClick(station)} key={station.station_id} />
    ))}
  </ContentLayout>
);

export default Content;
