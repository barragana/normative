import { Cell, RowLayout } from "./components";
import { cells } from "./utils";

const Row = ({ onClick, ...props }) => (
  <RowLayout onClick={onClick}>
    {cells.map(cell => (
      <Cell key={`${cell.key}_${props.station_id}`}>
        {props[cell.key]}
      </Cell>
    ))}
  </RowLayout>
);

export default Row;
