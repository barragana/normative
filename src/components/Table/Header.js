import { order } from '../../common';
import { Cell, RowLayout, SortIcon } from './components';
import { cells } from './utils';

const Header = ({ onClick, sortOptions = {} }) => (
  <RowLayout>
    {cells.map(cell => (
      <Cell key={cell.key} onClick={() => onClick(cell.key)}>
        {cell.label}
        {sortOptions.key === cell.key && (
          <SortIcon
            icon={SortIcon.icons.ARROW}
            rotate={sortOptions.order === order.ASC}
          />
        )}
      </Cell>
    ))}
  </RowLayout>
);

export default Header;
