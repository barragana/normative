import { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';

import ProcessingOverlay from '../ProcessingOverlay';

import Content from './Content';
import Header from './Header';

import { setOrder, sortData } from './utils';

export const TableLayout = styled.div`
  display: grid;
  grid-template-rows: 70px auto;
  row-gap: ${({ theme }) => theme.spacing.s};
  `;

const Table = ({ data, onSelectClick }) => {
  const [sortOptions, changeSortOptions] = useState({ key: '', order: '' });
  const [sortedData, setSortedData] = useState(data);
  const [isSorting, setIsSorting] = useState(false);

  useEffect(() => {
    setIsSorting(false);
    setSortedData(sortData(sortOptions, data));
  }, [sortOptions, data]);

  const handleSortClick = useCallback(key => {
    setIsSorting(true);
    changeSortOptions({ key, order: setOrder(key, sortOptions) });
  }, [sortOptions]);

  return (
    <TableLayout>
      {isSorting && (<ProcessingOverlay>Sorting...</ProcessingOverlay>)}
      <Header onClick={handleSortClick} sortOptions={sortOptions} />
      <Content data={sortedData} onClick={onSelectClick} />
    </TableLayout>
  );
};

export default Table;
