import styled, { css } from "styled-components";

import { withClickableStyle } from "../../styles/utils";
import Icon from "../Icon/Icon";

export const withRowStyle = css`
  align-items: center;
  column-gap: ${({ theme }) => theme.spacing.xs};
  display: grid;
  grid-template-columns: 120px 300px 300px 100px 100px 100px;
  justify-content: center;
  border-bottom: 1px solid ${({ theme }) => theme.colors.n5};

  &::last-child {
    border-bottom: none;
  }
`;

export const RowLayout = styled.div`
  ${withClickableStyle};
  ${withRowStyle};
`;

export const Cell = styled.div`
  align-items: center;
  display: grid;
  grid-template-columns: repeat(2, auto);
  padding: ${({ theme }) => theme.spacing.l};
  ${withClickableStyle};
`;

export const SortIcon = styled(Icon)`
  fill: ${({ theme }) => theme.colors.n9};
  ${({ rotate }) => rotate && 'transform: rotate(180deg)'};
`;
