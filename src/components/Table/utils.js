import { order } from "../../common";

export const cells = [
  { label: 'Station Id', key: 'station_id' },
  { label: 'Name', key: 'name' },
  { label: 'Address', key: 'address' },
  { label: 'Bikes Available', key: 'num_bikes_available' },
  { label: 'Docks Available', key: 'num_docks_available' },
  { label: 'Station Status', key: 'status' },
];

export const setOrder = (key, sortOptions) => {
  if (key === sortOptions.key && sortOptions.order === order.ASC) {
    return order.DESC;
  }
  return order.ASC;
}

export const sortData = (sortOptions, data) => {
  if (!sortOptions.key) return data;

  const dataSortedAsc = data.sort((a, b) => {
    if (a[sortOptions.key] > b[sortOptions.key]) {
      return 1;
    }
    if (a[sortOptions.key] < b[sortOptions.key]) {
      return -1;
    }
    return 0;
  });

  if (sortOptions.order === order.ASC) return dataSortedAsc;
  return dataSortedAsc.reverse();
}
