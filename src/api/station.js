import request from "./request";

export function getStationInformation() {
  return request('/station_information');
}

export function getStationStatus () {
  return request('/station_status');
}
