async function request(url, options) {
  const path = `${process.env.REACT_APP_API_BASE_URL_V1_EN}${url}`;
  let res = null;
  try {
    res = await fetch(`${path}`, options);
    const result = await res.json();
    if (!res.ok) {
      const error = new Error(res.statusText);
      error.status = res.status;
      error.errors = result || {};
      throw error;
    }
    return result;
  } catch (error) {
    error.errors = error.errors || {};
    throw error;
  }
}

export default request;
