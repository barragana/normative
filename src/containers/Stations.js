import React, { useState, useCallback } from 'react';
import styled from 'styled-components';

import ProcessingOverlay from '../components/ProcessingOverlay';
import Table from '../components/Table/Table';

import useStations from '../hooks/useStations';

const Layout = styled.div``;

const Stations = () => {
  const { data, isFetching, didInvalidate } = useStations();
  const [selectedStation, changeSelectedStation] = useState();

  const handleSelectClick = useCallback(station => {
    changeSelectedStation(station);
  }, []);

  if (!data.length) return false;
  if (isFetching) return <ProcessingOverlay>Fetching...</ProcessingOverlay>;
  if (didInvalidate) return <ProcessingOverlay>Something went wrong</ProcessingOverlay>;

  return (
    <Layout>
      <Table
        data={data}
        onSelectClick={handleSelectClick}
        selected={selectedStation}
      />
    </Layout>
  );
};

export default Stations;
