const { mapStations } = require("./useStations");

const status = [
  {
      station_id: "7000",
      num_bikes_available: 11,
      num_bikes_available_types: {
        mechanical: 11,
        ebike: 0
      },
      num_bikes_disabled: 0,
      num_docks_available: 24,
      num_docks_disabled: 0,
      is_installed: 1,
      is_renting: 1,
      is_returning: 1,
      last_reported: 1604257423,
      is_charging_station: false,
      status: "IN_SERVICE"
    },
    {
      station_id: "7001",
      num_bikes_available: 5,
      num_bikes_available_types: {
        mechanical: 5,
        ebike: 0
      },
      num_bikes_disabled: 0,
      num_docks_available: 10,
      num_docks_disabled: 0,
      is_installed: 1,
      is_renting: 1,
      is_returning: 1,
      last_reported: 1604257514,
      is_charging_station: false,
      status: "IN_SERVICE"
    },
    {
      station_id: "7002",
      num_bikes_available: 13,
      num_bikes_available_types: {
        mechanical: 13,
        ebike: 0
      },
      num_bikes_disabled: 0,
      num_docks_available: 6,
      num_docks_disabled: 0,
      is_installed: 1,
      is_renting: 1,
      is_returning: 1,
      last_reported: 1604257405,
      is_charging_station: false,
      status: "IN_SERVICE"
    },
    {
      station_id: "7003",
      num_bikes_available: 5,
      num_bikes_available_types: {
        mechanical: 5,
        ebike: 0
      },
      num_bikes_disabled: 0,
      num_docks_available: 10,
      num_docks_disabled: 0,
      is_installed: 1,
      is_renting: 1,
      is_returning: 1,
      last_reported: 1604257453,
      is_charging_station: false,
      status: "IN_SERVICE"
    },
    {
      station_id: "7004",
      num_bikes_available: 7,
      num_bikes_available_types: {
        mechanical: 7,
        ebike: 0
      },
      num_bikes_disabled: 0,
      num_docks_available: 4,
      num_docks_disabled: 0,
      is_installed: 1,
      is_renting: 1,
      is_returning: 1,
      last_reported: 1604257618,
      is_charging_station: false,
      status: "IN_SERVICE"
    },
    {
      station_id: "7005",
      num_bikes_available: 2,
      num_bikes_available_types: {
        mechanical: 2,
        ebike: 0
      },
      num_bikes_disabled: 1,
      num_docks_available: 16,
      num_docks_disabled: 0,
      is_installed: 1,
      is_renting: 1,
      is_returning: 1,
      last_reported: 1604257592,
      is_charging_station: false,
      status: "IN_SERVICE"
    },
    {
      station_id: "7006",
      num_bikes_available: 9,
      num_bikes_available_types: {
        mechanical: 9,
        ebike: 0
      },
      num_bikes_disabled: 0,
      num_docks_available: 2,
      num_docks_disabled: 0,
      is_installed: 1,
      is_renting: 1,
      is_returning: 1,
      last_reported: 1604257402,
      is_charging_station: false,
      status: "IN_SERVICE"
    },
];

const information = [
  {
    station_id: "7000",
    name: "Fort York Blvd / Capreol Ct",
    physical_configuration: "REGULAR",
    lat: 43.639832,
    lon: -79.395954,
    altitude: 0,
    address: "Fort York Blvd / Capreol Ct",
    capacity: 35,
    rental_methods: [
      "KEY",
      "CREDITCARD",
      "TRANSITCARD",
      "PHONE"
    ],
    groups: [ ],
    obcn: "647-643-9607",
    nearby_distance: 500
  },
  {
    station_id: "7001",
    name: "Lower Jarvis St / The Esplanade",
    physical_configuration: "REGULAR",
    lat: 43.64783,
    lon: -79.370698,
    altitude: 0,
    address: "Lower Jarvis St / The Esplanade",
    post_code: "M5E 1R8",
    capacity: 15,
    rental_methods: [
      "KEY",
      "CREDITCARD",
      "TRANSITCARD",
      "PHONE"
    ],
    groups: [ ],
    obcn: "416-617-9576",
    nearby_distance: 500
  },
  {
    station_id: "7002",
    name: "St. George St / Bloor St W",
    physical_configuration: "REGULAR",
    lat: 43.667333,
    lon: -79.399429,
    altitude: 0,
    address: "St. George St / Bloor St W",
    capacity: 19,
    rental_methods: [
      "KEY",
      "CREDITCARD",
      "TRANSITCARD",
      "PHONE"
    ],
    groups: [ ],
    obcn: "647-643-9615",
    nearby_distance: 500
  },
  {
    station_id: "7003",
    name: "Madison Ave / Bloor St W",
    physical_configuration: "REGULAR",
    lat: 43.667158,
    lon: -79.402761,
    altitude: null,
    address: "Madison Ave / Bloor St W",
    capacity: 15,
    rental_methods: [
      "KEY",
      "CREDITCARD",
      "TRANSITCARD",
      "PHONE"
    ],
    groups: [ ],
    obcn: "647-631-4587",
    nearby_distance: 500
  },
  {
    station_id: "7004",
    name: "University Ave / Elm St",
    physical_configuration: "REGULAR",
    lat: 43.656518,
    lon: -79.389099,
    altitude: null,
    address: "University Ave / Elm St",
    capacity: 11,
    rental_methods: [
      "KEY",
      "CREDITCARD",
      "TRANSITCARD",
      "PHONE"
    ],
    groups: [
      "P7004-7047"
    ],
    obcn: "647-643-9673",
    nearby_distance: 500
  },
  {
    station_id: "7005",
    name: "King St W / York St",
    physical_configuration: "REGULAR",
    lat: 43.6480008,
    lon: -79.383177,
    altitude: 0,
    address: "King St W / York St",
    capacity: 19,
    rental_methods: [
      "KEY",
      "CREDITCARD",
      "TRANSITCARD",
      "PHONE"
    ],
    groups: [ ],
    obcn: "647-643-9693",
    nearby_distance: 500
  },
  {
    station_id: "7006",
    name: "Bay St / College St (East Side)",
    physical_configuration: "REGULAR",
    lat: 43.660439,
    lon: -79.385525,
    altitude: 0,
    address: "Bay St / College St (East Side)",
    capacity: 11,
    rental_methods: [
      "KEY",
      "CREDITCARD",
      "TRANSITCARD",
      "PHONE"
    ],
    groups: [
      "P7006-7235"
    ],
    obcn: "416-569-2349",
    nearby_distance: 500
  },
]
const result = [{"station_id":"7000","name":"Fort York Blvd / Capreol Ct","physical_configuration":"REGULAR","lat":43.639832,"lon":-79.395954,"altitude":0,"address":"Fort York Blvd / Capreol Ct","capacity":35,"rental_methods":["KEY","CREDITCARD","TRANSITCARD","PHONE"],"groups":[],"obcn":"647-643-9607","nearby_distance":500,"num_bikes_available":11,"num_bikes_available_types":{"mechanical":11,"ebike":0},"num_bikes_disabled":0,"num_docks_available":24,"num_docks_disabled":0,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1604257423,"is_charging_station":false,"status":"IN_SERVICE"},{"station_id":"7001","name":"Lower Jarvis St / The Esplanade","physical_configuration":"REGULAR","lat":43.64783,"lon":-79.370698,"altitude":0,"address":"Lower Jarvis St / The Esplanade","post_code":"M5E 1R8","capacity":15,"rental_methods":["KEY","CREDITCARD","TRANSITCARD","PHONE"],"groups":[],"obcn":"416-617-9576","nearby_distance":500,"num_bikes_available":5,"num_bikes_available_types":{"mechanical":5,"ebike":0},"num_bikes_disabled":0,"num_docks_available":10,"num_docks_disabled":0,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1604257514,"is_charging_station":false,"status":"IN_SERVICE"},{"station_id":"7002","name":"St. George St / Bloor St W","physical_configuration":"REGULAR","lat":43.667333,"lon":-79.399429,"altitude":0,"address":"St. George St / Bloor St W","capacity":19,"rental_methods":["KEY","CREDITCARD","TRANSITCARD","PHONE"],"groups":[],"obcn":"647-643-9615","nearby_distance":500,"num_bikes_available":13,"num_bikes_available_types":{"mechanical":13,"ebike":0},"num_bikes_disabled":0,"num_docks_available":6,"num_docks_disabled":0,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1604257405,"is_charging_station":false,"status":"IN_SERVICE"},{"station_id":"7003","name":"Madison Ave / Bloor St W","physical_configuration":"REGULAR","lat":43.667158,"lon":-79.402761,"altitude":null,"address":"Madison Ave / Bloor St W","capacity":15,"rental_methods":["KEY","CREDITCARD","TRANSITCARD","PHONE"],"groups":[],"obcn":"647-631-4587","nearby_distance":500,"num_bikes_available":5,"num_bikes_available_types":{"mechanical":5,"ebike":0},"num_bikes_disabled":0,"num_docks_available":10,"num_docks_disabled":0,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1604257453,"is_charging_station":false,"status":"IN_SERVICE"},{"station_id":"7004","name":"University Ave / Elm St","physical_configuration":"REGULAR","lat":43.656518,"lon":-79.389099,"altitude":null,"address":"University Ave / Elm St","capacity":11,"rental_methods":["KEY","CREDITCARD","TRANSITCARD","PHONE"],"groups":["P7004-7047"],"obcn":"647-643-9673","nearby_distance":500,"num_bikes_available":7,"num_bikes_available_types":{"mechanical":7,"ebike":0},"num_bikes_disabled":0,"num_docks_available":4,"num_docks_disabled":0,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1604257618,"is_charging_station":false,"status":"IN_SERVICE"},{"station_id":"7005","name":"King St W / York St","physical_configuration":"REGULAR","lat":43.6480008,"lon":-79.383177,"altitude":0,"address":"King St W / York St","capacity":19,"rental_methods":["KEY","CREDITCARD","TRANSITCARD","PHONE"],"groups":[],"obcn":"647-643-9693","nearby_distance":500,"num_bikes_available":2,"num_bikes_available_types":{"mechanical":2,"ebike":0},"num_bikes_disabled":1,"num_docks_available":16,"num_docks_disabled":0,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1604257592,"is_charging_station":false,"status":"IN_SERVICE"},{"station_id":"7006","name":"Bay St / College St (East Side)","physical_configuration":"REGULAR","lat":43.660439,"lon":-79.385525,"altitude":0,"address":"Bay St / College St (East Side)","capacity":11,"rental_methods":["KEY","CREDITCARD","TRANSITCARD","PHONE"],"groups":["P7006-7235"],"obcn":"416-569-2349","nearby_distance":500,"num_bikes_available":9,"num_bikes_available_types":{"mechanical":9,"ebike":0},"num_bikes_disabled":0,"num_docks_available":2,"num_docks_disabled":0,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1604257402,"is_charging_station":false,"status":"IN_SERVICE"}];
describe('Hooks', () => {
  describe('Map Stations should', () => {
    test('return an array of composed item from information and status', () => {
      expect(mapStations({ information, status })).toStrictEqual(result);
    })
  })
});
