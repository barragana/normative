import { useReducer, useEffect } from 'react';

import { getStationInformation, getStationStatus } from '../api/station';

const initialState = {
  isFetching: false,
  didInvalidate: false,
  data: [],
  error: null,
};

export function reducer(state, action) {
  switch (action.type) {
    case 'FETCH_STATIONS_REQUESTED':
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case 'FETCH_STATIONS_SUCCESSED':
      return {
        ...state,
        isFetching: false,
        data: action.stations,
      };
    case 'FETCH_STATIONS_FAILED':
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
        error: action.error,
      };
    default:
      throw new Error();
  }
}

export function mapStations ({ information, status }) {
  const mappedStatus = status.reduce((res, status) => {
    res[status.station_id] = status;
    return res;
  }, {});

  return information.map(station => ({
    ...station,
    ...mappedStatus[station.station_id],
  }));
};

function useStations() {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    dispatch({type: 'FETCH_STATIONS_REQUESTED' });
  }, []);

  useEffect(() => {
    Promise.all([
      getStationInformation(),
      getStationStatus(),
    ]).then(([ information, status ]) => {
      const stations = mapStations({
        information: information.data.stations,
        status: status.data.stations,
      });
      dispatch({type: 'FETCH_STATIONS_SUCCESSED', stations });
    });
  }, []);

  return state;
}

export default useStations;
